from rest_framework import serializers

from core.models import Quran, Chapter, Verse


class QuranSerializer(serializers.ModelSerializer):
    """Serializer for Quran translations"""

    class Meta:
        model = Quran
        fields = ('id', 'translation')


class ChapterSerializer(serializers.ModelSerializer):
    """Serializer for chapters of the Quran"""

    class Meta:
        model = Chapter
        fields = ('chapter_number', 'chapter_name')


class VerseSerializer(serializers.ModelSerializer):
    """Serializer for verses of the chapters of the Quran"""
    chapter = serializers.PrimaryKeyRelatedField(many=True, queryset=Chapter.objects.all())

    class Meta:
        model = Verse
        fields = ('verse_number', 'verse_text', 'chapter')
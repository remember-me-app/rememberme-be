from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Quran, User

QURAN_URL = reverse('quran:quran-list')


class PublicQuranApiTests(TestCase):
    """Test the publicly available Quran API"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for adding entries"""
        response = self.client.post(QURAN_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateQuranApiTests(TestCase):
    """Test authorised Quran API"""

    def setUp(self):
        self.user = User.objects.create_user(
            'tester@mail.com',
            'password123'
        )
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_retrieve_quran(self):
        """Test retrieving quran"""
        Quran.objects.create(translation='Saheeh International', translator='Saheeh Team')
        Quran.objects.create(translation='Classical', translator='Mohammed Pickthall')

        response = self.client.get(QURAN_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_quran_entry_successful(self):
        """Test creating a new quran entry"""

        payload = {'translator': 'Saheeh Team',
                   'translation': 'Saheeh International'}
        self.client.post(QURAN_URL, payload)

        exists = Quran.objects.filter(
            translation=payload['translation']
        ).exists()

        self.assertTrue(exists)

    def test_create_quran_entry_invalid(self):
        """Test create a new quran entry with invalid payload"""
        payload={'translation':  ''}
        response = self.client.post(QURAN_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Quran, Chapter, User

CHAPTER_URL = reverse('quran:chapter-list')


class PublicChapterApiTests(TestCase):
    """Test the publicly available Quran API"""

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """Test that login is required for adding entries"""
        response = self.client.post(CHAPTER_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


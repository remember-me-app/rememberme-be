from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import QuranViewSet, ChapterViewSet

app_name = 'quran'

router = DefaultRouter()
router.register(r'quran', QuranViewSet)
router.register(r'chapters', ChapterViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import generics

from django.shortcuts import get_object_or_404


from core.models import Quran, Chapter, Verse
from .serializers import QuranSerializer, ChapterSerializer, VerseSerializer


class QuranViewSet(ModelViewSet):
    """Manage translations in the database"""

    queryset = Quran.objects.all()
    serializer_class = QuranSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class ChapterViewSet(ModelViewSet, NestedViewSetMixin):
    """Manage chapters in the database"""

    queryset = Chapter.objects.all()
    serializer_class = ChapterSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @action(detail=True)
    def verses(self, request, pk=None):
        queryset = Chapter.objects.all()
        chapter = get_object_or_404(queryset, pk=pk)
        verse_queryset = Verse.objects.filter(chapter=chapter.chapter_number)
        return Response([verse.verse_text for verse in verse_queryset])


class VerseList(generics.ListAPIView):
    """Manage verses in the database"""

    queryset = Verse.objects.all()
    serializer_class = VerseSerializer

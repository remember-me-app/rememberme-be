create table django_migrations
(
    id      serial                   not null
        constraint django_migrations_pkey
            primary key,
    app     varchar(255)             not null,
    name    varchar(255)             not null,
    applied timestamp with time zone not null
);

create table django_content_type
(
    id        serial       not null
        constraint django_content_type_pkey
            primary key,
    app_label varchar(100) not null,
    model     varchar(100) not null,
    constraint django_content_type_app_label_model_76bd3d3b_uniq
        unique (app_label, model)
);

create table auth_permission
(
    id              serial       not null
        constraint auth_permission_pkey
            primary key,
    name            varchar(255) not null,
    content_type_id integer      not null
        constraint auth_permission_content_type_id_2f476e4b_fk_django_co
            references django_content_type
            deferrable initially deferred,
    codename        varchar(100) not null,
    constraint auth_permission_content_type_id_codename_01ab375a_uniq
        unique (content_type_id, codename)
);

create index auth_permission_content_type_id_2f476e4b
    on auth_permission (content_type_id);

create table auth_group
(
    id   serial       not null
        constraint auth_group_pkey
            primary key,
    name varchar(150) not null
        constraint auth_group_name_key
            unique
);

create index auth_group_name_a6ea08ec_like
    on auth_group (name);

create table auth_group_permissions
(
    id            serial  not null
        constraint auth_group_permissions_pkey
            primary key,
    group_id      integer not null
        constraint auth_group_permissions_group_id_b120cbf9_fk_auth_group_id
            references auth_group
            deferrable initially deferred,
    permission_id integer not null
        constraint auth_group_permissio_permission_id_84c5c92e_fk_auth_perm
            references auth_permission
            deferrable initially deferred,
    constraint auth_group_permissions_group_id_permission_id_0cd325b0_uniq
        unique (group_id, permission_id)
);

create index auth_group_permissions_group_id_b120cbf9
    on auth_group_permissions (group_id);

create index auth_group_permissions_permission_id_84c5c92e
    on auth_group_permissions (permission_id);

create table "user"
(
    id           serial                   not null
        constraint user_pkey
            primary key,
    is_superuser boolean                  not null,
    first_name   varchar(255)             not null,
    last_name    varchar(255)             not null,
    login_name   varchar(100)             not null,
    screen_name  varchar(100)             not null,
    email        varchar(255)             not null
        constraint user_email_key
            unique,
    password     varchar(255)             not null,
    date_joined  date                     not null,
    last_login   timestamp with time zone not null,
    is_staff     boolean                  not null,
    is_active    boolean                  not null,
    intention    text                     not null
);

create index user_email_54dc62b2_like
    on "user" (email);

create table user_groups
(
    id       serial  not null
        constraint user_groups_pkey
            primary key,
    user_id  integer not null
        constraint user_groups_user_id_abaea130_fk_user_id
            references "user"
            deferrable initially deferred,
    group_id integer not null
        constraint user_groups_group_id_b76f8aba_fk_auth_group_id
            references auth_group
            deferrable initially deferred,
    constraint user_groups_user_id_group_id_40beef00_uniq
        unique (user_id, group_id)
);

create index user_groups_user_id_abaea130
    on user_groups (user_id);

create index user_groups_group_id_b76f8aba
    on user_groups (group_id);

create table user_user_permissions
(
    id            serial  not null
        constraint user_user_permissions_pkey
            primary key,
    user_id       integer not null
        constraint user_user_permissions_user_id_ed4a47ea_fk_user_id
            references "user"
            deferrable initially deferred,
    permission_id integer not null
        constraint user_user_permission_permission_id_9deb68a3_fk_auth_perm
            references auth_permission
            deferrable initially deferred,
    constraint user_user_permissions_user_id_permission_id_7dc6e2e0_uniq
        unique (user_id, permission_id)
);

create index user_user_permissions_user_id_ed4a47ea
    on user_user_permissions (user_id);

create index user_user_permissions_permission_id_9deb68a3
    on user_user_permissions (permission_id);

create table django_admin_log
(
    id              serial                   not null
        constraint django_admin_log_pkey
            primary key,
    action_time     timestamp with time zone not null,
    object_id       text,
    object_repr     varchar(200)             not null,
    action_flag     smallint                 not null
        constraint django_admin_log_action_flag_check
            check (action_flag >= 0),
    change_message  text                     not null,
    content_type_id integer
        constraint django_admin_log_content_type_id_c4bce8eb_fk_django_co
            references django_content_type
            deferrable initially deferred,
    user_id         integer                  not null
        constraint django_admin_log_user_id_c564eba6_fk_user_id
            references "user"
            deferrable initially deferred
);

create index django_admin_log_content_type_id_c4bce8eb
    on django_admin_log (content_type_id);

create index django_admin_log_user_id_c564eba6
    on django_admin_log (user_id);

create table django_session
(
    session_key  varchar(40)              not null
        constraint django_session_pkey
            primary key,
    session_data text                     not null,
    expire_date  timestamp with time zone not null
);

create index django_session_session_key_c0390e0f_like
    on django_session (session_key);

create index django_session_expire_date_a5c62663
    on django_session (expire_date);

create table buddy_relationship
(
    id   serial       not null
        constraint buddy_relationship_pkey
            primary key,
    name varchar(250) not null
);

create table buddy_status
(
    id   serial       not null
        constraint buddy_status_pkey
            primary key,
    name varchar(250) not null
);

create table mood
(
    name         varchar(250) not null
        constraint mood_pkey
            primary key,
    mood_meaning varchar(250) not null
);

create index mood_name_b60d4618_like
    on mood (name);

create table quran
(
    id          serial       not null
        constraint quran_pkey
            primary key,
    translation varchar(250) not null,
    translator  varchar(250) not null
);

create table chapter
(
    chapter_number integer      not null
        constraint chapter_pkey
            primary key,
    chapter_name   varchar(250) not null,
    translation_id integer      not null
        constraint chapter_translation_id_9540b7fa_fk_quran_id
            references quran
            deferrable initially deferred
);

create index chapter_translation_id_9540b7fa
    on chapter (translation_id);

create table theme
(
    name varchar(250) not null
        constraint theme_pkey
            primary key,
    id   integer      not null
        constraint theme_id_key
            unique
);

create index theme_name_336c9eb7_like
    on theme (name);

create table user_buddy
(
    id                    serial  not null
        constraint user_buddy_pkey
            primary key,
    befriended_at         date    not null,
    buddy_id              integer not null
        constraint user_buddy_buddy_id_2c8dc925_fk_user_id
            references "user"
            deferrable initially deferred,
    buddy_relationship_id integer not null
        constraint user_buddy_buddy_relationship_i_f17324c9_fk_buddy_rel
            references buddy_relationship
            deferrable initially deferred,
    buddy_status_id       integer not null
        constraint user_buddy_buddy_status_id_4641881b_fk_buddy_status_id
            references buddy_status
            deferrable initially deferred,
    user_id               integer not null
        constraint user_buddy_user_id_83afccdc_fk_user_id
            references "user"
            deferrable initially deferred
);

create index user_buddy_buddy_id_2c8dc925
    on user_buddy (buddy_id);

create index user_buddy_buddy_relationship_id_f17324c9
    on user_buddy (buddy_relationship_id);

create index user_buddy_buddy_status_id_4641881b
    on user_buddy (buddy_status_id);

create index user_buddy_user_id_83afccdc
    on user_buddy (user_id);

create table themed_verse
(
    id                   serial       not null
        constraint themed_verse_pkey
            primary key,
    verse_number         integer      not null,
    ponder               text         not null
        constraint themed_verse_ponder_key
            unique,
    prompt               text         not null
        constraint themed_verse_prompt_key
            unique,
    dua                  text         not null,
    his_name             text         not null,
    his_name_translation text         not null,
    chapter_id           integer      not null
        constraint themed_verse_chapter_id_e896de3e_fk_chapter_chapter_number
            references chapter
            deferrable initially deferred,
    theme_id             varchar(250) not null
        constraint themed_verse_theme_id_9e3a521f_fk_theme_name
            references theme
            deferrable initially deferred
);

create index themed_verse_ponder_46ceeb29_like
    on themed_verse (ponder);

create index themed_verse_prompt_6f894674_like
    on themed_verse (prompt);

create index themed_verse_chapter_id_e896de3e
    on themed_verse (chapter_id);

create index themed_verse_theme_id_9e3a521f
    on themed_verse (theme_id);

create index themed_verse_theme_id_9e3a521f_like
    on themed_verse (theme_id);

create table journal
(
    id           serial       not null
        constraint journal_pkey
            primary key,
    created_date date         not null
        constraint journal_created_date_key
            unique,
    entry_text   text         not null
        constraint journal_entry_text_key
            unique,
    author_id    integer      not null
        constraint journal_author_id_cc1a24f0_fk_user_id
            references "user"
            deferrable initially deferred,
    mood_id      varchar(250) not null
        constraint journal_mood_id_6f54dabb_fk_mood_name
            references mood
            deferrable initially deferred
);

create table entry_category
(
    id            serial       not null
        constraint entry_category_pkey
            primary key,
    author_id     integer      not null
        constraint entry_category_author_id_3ac73b0b_fk_user_id
            references "user"
            deferrable initially deferred,
    entry_text_id text         not null
        constraint entry_category_entry_text_id_7de57aaa_fk_journal_entry_text
            references journal (entry_text)
            deferrable initially deferred,
    theme_id      varchar(250) not null
        constraint entry_category_theme_id_20e3a988_fk_theme_name
            references theme
            deferrable initially deferred
);

create index entry_category_author_id_3ac73b0b
    on entry_category (author_id);

create index entry_category_entry_text_id_7de57aaa
    on entry_category (entry_text_id);

create index entry_category_entry_text_id_7de57aaa_like
    on entry_category (entry_text_id);

create index entry_category_theme_id_20e3a988
    on entry_category (theme_id);

create index entry_category_theme_id_20e3a988_like
    on entry_category (theme_id);

create index journal_entry_text_19a64c64_like
    on journal (entry_text);

create index journal_author_id_cc1a24f0
    on journal (author_id);

create index journal_mood_id_6f54dabb
    on journal (mood_id);

create index journal_mood_id_6f54dabb_like
    on journal (mood_id);

create table favorite_verse
(
    id            serial  not null
        constraint favorite_verse_pkey
            primary key,
    verse_content text    not null,
    user_id       integer not null
        constraint favorite_verse_user_id_4841c9ef_fk_user_id
            references "user"
            deferrable initially deferred
);

create index favorite_verse_user_id_4841c9ef
    on favorite_verse (user_id);

create table favorite_prompt
(
    id      serial  not null
        constraint favorite_prompt_pkey
            primary key,
    prompt  text    not null,
    user_id integer not null
        constraint favorite_prompt_user_id_9868ff90_fk_user_id
            references "user"
            deferrable initially deferred
);

create index favorite_prompt_user_id_9868ff90
    on favorite_prompt (user_id);

create table favorite_ponder
(
    id      serial  not null
        constraint favorite_ponder_pkey
            primary key,
    ponder  text    not null,
    user_id integer not null
        constraint favorite_ponder_user_id_4961e712_fk_user_id
            references "user"
            deferrable initially deferred
);

create index favorite_ponder_user_id_4961e712
    on favorite_ponder (user_id);

create table favorite_journal
(
    id                serial  not null
        constraint favorite_journal_pkey
            primary key,
    author_id         integer not null
        constraint favorite_journal_author_id_118378b8_fk_user_id
            references "user"
            deferrable initially deferred,
    created_date_id   date    not null
        constraint favorite_journal_created_date_id_55d21283_fk_journal_c
            references journal (created_date)
            deferrable initially deferred,
    favorite_entry_id text    not null
        constraint favorite_journal_favorite_entry_id_b9cdcc7c_fk_journal_e
            references journal (entry_text)
            deferrable initially deferred
);

create index favorite_journal_author_id_118378b8
    on favorite_journal (author_id);

create index favorite_journal_created_date_id_55d21283
    on favorite_journal (created_date_id);

create index favorite_journal_favorite_entry_id_b9cdcc7c
    on favorite_journal (favorite_entry_id);

create index favorite_journal_favorite_entry_id_b9cdcc7c_like
    on favorite_journal (favorite_entry_id);

create table favorite_dua
(
    id      serial  not null
        constraint favorite_dua_pkey
            primary key,
    dua     text    not null,
    user_id integer not null
        constraint favorite_dua_user_id_d5ea83f5_fk_user_id
            references "user"
            deferrable initially deferred
);

create index favorite_dua_user_id_d5ea83f5
    on favorite_dua (user_id);

create table verse
(
    id           serial  not null
        constraint verse_pkey
            primary key,
    verse_number integer not null,
    verse_text   text    not null,
    chapter_id   integer not null
        constraint verse_chapter_id_5d58cb47_fk_chapter_chapter_number
            references chapter
            deferrable initially deferred,
    constraint verse_chapter_id_verse_number_2813d634_uniq
        unique (chapter_id, verse_number)
);

create index verse_chapter_id_5d58cb47
    on verse (chapter_id);



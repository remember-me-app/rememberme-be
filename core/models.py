from django.db import models
from django.conf import settings
from django.utils.timezone import now

from django.contrib.auth.models import AbstractBaseUser, \
    BaseUserManager, PermissionsMixin


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """Create and saves a new user"""
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self.db)

        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self.db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model supports using email instead of username"""

    class Meta:
        db_table = "user"

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    login_name = models.CharField(max_length=100, blank=True)
    screen_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    date_joined = models.DateField(null=False, auto_now_add=True)
    last_login = models.DateTimeField(default=now, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    intention = models.TextField(blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'


class Quran(models.Model):

    class Meta:
        db_table = "quran"

    translation = models.CharField(max_length=250, null=False,blank=False)
    translator = models.CharField(max_length=250, null=False, blank=False)

    def __str__(self):
        return self.translator


class Chapter(models.Model):

    class Meta:
        db_table = "chapter"
        ordering = ['chapter_number']

    chapter_number = models.IntegerField(primary_key=True, unique=True)
    chapter_name = models.CharField(max_length=250)
    translation = models.ForeignKey(Quran, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.chapter_number}'


class Theme(models.Model):

    class Meta:
        db_table = "theme"

    name = models.CharField(max_length=250, primary_key=True)
    id = models.IntegerField(unique=True, default=1)

    def __str__(self):
        return self.name


class Verse(models.Model):

    class Meta:
        db_table = "verse"
        unique_together = ('chapter', 'verse_number')
        ordering = ['chapter', 'verse_number']

    chapter = models.ForeignKey(Chapter, related_name='verses', on_delete=models.CASCADE)
    verse_number = models.IntegerField()
    verse_text = models.TextField()

    def __str__(self):
        return self.verse_text


class ThemedVerse(models.Model):

    class Meta:
        db_table = "themed_verse"
        indexes = [
            models.Index(fields=['chapter_id', 'verse_number', 'ponder', 'prompt'], name='PrayerTheme',
                         condition=models.Q(theme_id='prayer'))
        ]

    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE, default=0, blank=True)
    verse_number = models.IntegerField(default=0, blank=True)
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE)
    ponder = models.TextField(unique=True)
    prompt = models.TextField(unique=True)
    dua = models.TextField(blank=True)
    his_name = models.TextField(blank=True)
    his_name_translation = models.TextField(blank=True)


class Mood(models.Model):

    class Meta:
        db_table = "mood"

    name = models.CharField(max_length=250, primary_key=True)
    mood_meaning = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class FavoriteVerse(models.Model):

    class Meta:
        db_table = "favorite_verse"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    verse_content = models.TextField()


class FavoritePonder(models.Model):

    class Meta:
        db_table = "favorite_ponder"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ponder = models.TextField()


class FavoritePrompt(models.Model):

    class Meta:
        db_table = "favorite_prompt"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    prompt = models.TextField()


class FavoriteDua(models.Model):

    class Meta:
        db_table = "favorite_dua"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    dua = models.TextField()


class BuddyStatus(models.Model):

    class Meta:
        db_table = "buddy_status"

    name = models.CharField(max_length=250)


class BuddyRelationship(models.Model):

    class Meta:
        db_table = "buddy_relationship"

    name = models.CharField(max_length=250)


class UserBuddy(models.Model):

    class Meta:
        db_table = "user_buddy"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    buddy = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_buddy")
    befriended_at = models.DateField()
    buddy_status = models.ForeignKey(BuddyStatus, on_delete=models.CASCADE)
    buddy_relationship = models.ForeignKey(BuddyRelationship, on_delete=models.CASCADE)


class Journal(models.Model):

    class Meta:
        db_table = "journal"

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_date = models.DateField(unique=True)
    entry_text = models.TextField(unique=True)
    mood = models.ForeignKey(Mood, on_delete=models.CASCADE)
    theme = models.ManyToManyField(Theme, through="EntryCategory")


class EntryCategory(models.Model):

    class Meta:
        db_table = "entry_category"

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    entry_text = models.ForeignKey(Journal, to_field="entry_text", on_delete=models.CASCADE)
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE)


class FavoriteJournal (models.Model):

    class Meta:
        db_table = "favorite_journal"

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    favorite_entry = models.ForeignKey(Journal, to_field="entry_text", on_delete=models.CASCADE,
                                       related_name="favourite_entry")
    created_date = models.ForeignKey(Journal, to_field="created_date", on_delete=models.CASCADE,
                                     related_name="entry_date")


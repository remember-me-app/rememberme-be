from django.test import TestCase
from model_bakery import baker

from datetime import datetime
from ..models import User, Quran, Theme, Verse


class ModelTests(TestCase):

    def test_create_user_with_email_successful(self):
        """Test creating user with new email has been successful"""
        email = 'test@remember-me.today'
        password = 'WakandaForever123'
        date_joined = datetime.now()

        user = User.objects.create_user(email=email,
                                        password=password,
                                        date_joined=date_joined)

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalised(self):
        """Test the email for a new user is normalised"""
        email = 'test@ICLOUD.COM'
        password = 'WakandaForever123'
        date_joined = datetime.now()

        user = User.objects.create_user(email=email,
                                        password=password,
                                        date_joined=date_joined)

        self.assertEqual(user.email, email.lower())

    def test_new_user_without_email(self):
        """Test creating user with no email raises error"""
        with self.assertRaises(ValueError):
            User.objects.create_user(None, 'WakandaForever123')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        email = 'test@ICLOUD.COM'
        password = 'WakandaForever123'
        date_joined = datetime.now()

        user = User.objects.create_superuser(
            email=email,
            password=password,
            date_joined=date_joined
        )
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_model_str(self):
        theme = Theme.objects.create(name='Love', id=5)
        quran = Quran.objects.create(translator='Saheeh Team', translation='Saheeh International')
        verse = baker.make(Verse, verse_text="Say, He is Allah, the One")

        self.assertEqual(str(verse), verse.verse_text)
        self.assertEqual(str(theme), theme.name)
        self.assertEqual(str(quran), quran.translator)


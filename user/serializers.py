from core.models import User, Journal
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers


class JournalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Journal
        fields = ['author', 'created_date', 'entry_text', 'mood']


class UserSerializer(serializers.ModelSerializer):
    """Serializer for User object"""
    # password2 = serializers.CharField(label='Confirm Password')

    class Meta:
        model = User
        fields = ('email', 'password', 'first_name')
        extra_kwargs = {'password': {'write_only': True, 'min_length': 8}}

    # def validate(self, password):
    #     if password != self.initial_data['password2']:
    #         raise ValidationError('Passwords must match')
    #
    #     return data

    def create(self, validated_data):
        """crete a new user with encrypted password and return it"""
        user = User.objects.create(**validated_data)
        return user

    def update(self, instance, validated_data):
        """Update a user, setting a password correctly and return it"""
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication object"""
    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        """Validate and authenticate the user"""
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password
        )

        if not user:
            message = _('Unable to authenticate with provided credentials')
            raise serializers.ValidationError(message, code='authentication')

        attrs['user'] = user
        return attrs

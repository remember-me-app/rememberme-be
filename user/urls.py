from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import UserViewSet, CreateTokenView, ManageUserView, JournalViewSet

app_name = 'user'

router = DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'journal', JournalViewSet)

urlpatterns = [
        path('', include(router.urls)),
        path('token/', CreateTokenView.as_view(), name='token'),
        path('me/', ManageUserView.as_view(), name='me'),
    ]

from rest_framework import serializers

from core.models import Theme, ThemedVerse


class ThemeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Theme
        fields = ('id', 'name')


class ThemedVerseSerializer(serializers.ModelSerializer):

    class Meta:
        model = ThemedVerse
        fields = ('chapter_id', 'verse_number', 'ponder', 'prompt')


class HisNamesThemeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ThemedVerse
        fields = ('his_name', 'his_name_translation', 'chapter_id', 'verse_number', 'ponder', 'prompt', 'dua')

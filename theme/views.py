from rest_framework import generics
from rest_framework import mixins
from rest_framework import filters

from .serializers import ThemeSerializer, ThemedVerseSerializer, HisNamesThemeSerializer
from core.models import Theme, ThemedVerse


class ThemeView(mixins.ListModelMixin, generics.GenericAPIView):
    filter_backends = (filters.SearchFilter, )
    serializer_class = ThemeSerializer
    queryset = Theme.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PrayerThemeView(mixins.ListModelMixin, generics.GenericAPIView):
    serializer_class = ThemedVerseSerializer
    queryset = ThemedVerse.objects.filter(theme_id='Prayer')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PatienceThemeView(mixins.ListModelMixin, generics.GenericAPIView):
    serializer_class = ThemedVerseSerializer
    queryset = ThemedVerse.objects.filter(theme_id='Patience')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ForgivenessThemeView(mixins.ListModelMixin, generics.GenericAPIView):
    serializer_class = ThemedVerseSerializer
    queryset = ThemedVerse.objects.filter(theme_id='Forgiveness and Repentance')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class HisNameThemeView(mixins.ListModelMixin, generics.GenericAPIView):
    serializer_class = HisNamesThemeSerializer
    queryset = ThemedVerse.objects.filter(theme_id='His Names')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
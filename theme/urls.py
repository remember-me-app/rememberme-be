from django.urls import path

from .views import ThemeView, PrayerThemeView, PatienceThemeView, ForgivenessThemeView, HisNameThemeView

app_name = 'theme'

urlpatterns = [
    path('', ThemeView.as_view(), name='theme'),
    path('prayer/', PrayerThemeView.as_view(), name='prayer-theme'),
    path('patience/', PatienceThemeView.as_view(), name='patience-theme'),
    path('forgiveness/', ForgivenessThemeView.as_view(), name='forgiveness-theme'),
    path('his-names/', HisNameThemeView.as_view(), name='his-names-theme'),
]

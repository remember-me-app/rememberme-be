# Remember Me: Relational Database

## Mission Objectives
- Store users’ email address and password 
- Store users’ journal entries
- Keep track of users’ frequency of app usage
- Store verses, chapters, pondering and prompts 
- Users should have access to journal entries and favourited items
- Users should be able to add other users to their buddies list 

## Entity-Relationship Diagram
The ER diagram for the database can be found [here](https://gitlab.com/remember-me-app/rememberme-be/-/blob/master/database%20files/ER-diagram.png).

## Database Schema
The database schema can be found [here](https://gitlab.com/remember-me-app/rememberme-be/-/blob/master/database%20files/database-schema.png).

## SQL commands to recreate database
SQL commands to recreate the database can be found [here](https://gitlab.com/remember-me-app/rememberme-be/-/blob/master/database%20files/database_script.sql)

## Backup Strategy
The database is backed up locally using the command:
`pg_dump /dbname/ > /dumpfile/`

This generates a file with SQL commands that when fed back to the server will recreate the database in the same state it was at the time of the dump. This method was chosen over the file-level backup because the database is not complex, and pg_dump takes us minimum amount of space. In addition, pgdump files can be outputted to newer versions of Postgres allowing for flexibility, whereas file-level and continuous archiving backup methods are version-specific. 

A cron task is used to automate backup in the following steps:
- Ensure you are logged in as the Postgres (or super) user 
- Create the folder you want to save the automatic backups in:
	`mkdir -p ~/postgres/backups`
- Edit the crontab to create a new cron task by running the command:
	`crontab -e`
- In the editor, paste the following command to automate backup at midnight on Sundays:
	`0 0 * * 0 pg_dump -U postgres dbname > ~/postgres/backups/dumpfile.bak`

Note: To change the schedule and frequency of the backup consult this guide. 

## Performance Measures
pg_stat_statements extension is used to measure performance by going through checking through query execution time and much more.
Filters are used on querysets to improve speed and efficiency

## Integration with Application
The database is accessed via Django which acts as an Object Relational Mapper (ORM). Data can be written to the database from the python shell or with the use of the RESTful API in the application. 

# Backend Application
Provides a RESTful API which serves data to, and retrieves data from the RememberMe relational database through the use of django models. It also provides a means to authenticate users via Token Authentication. The API is built with django-rest-framework. The Django *BaseUserManager* model was extended to create functions separating an administrated user from a regular user.
The application interacts with front-end clients via the API. 

The API documentation can be found [here](https://gitlab.com/remember-me-app/rememberme-be/-/wikis/home).

## App Architecture

The architecture for the application can be found [here](https://gitlab.com/remember-me-app/rememberme-be/-/blob/master/app-architecture.png).

## API Interface

The API interface, provided by the rest framework, displays a list of endpoints that can be accessed such as quran/, theme/, users/ and admin/.  For example, the user endpoint can be used to generate the token for authentication, create and retrieve new users, among other features. 

The API was designed based on the needs it would serve. First, a list of required resources were created, and endpoints were generated to load up each resource in json format (as listed in the API documentation). The use of django models and serialisers, makes the addition of new resources relatively easy. 

## Set up

### Prerequisites

The requirements necessary to run the code on your machine are:

- python3 and pip3

  - Check that you have python3 installed on your computer with the command: 

    `$ python --version` OR `$ python3 --version`

  - If you don’t have python3 installed, you may find these useful: 

    - https://www.python.org/
    - https://brew.sh/

### On Local Machine: Quick Start Guide

1. Create a new empty folder on your computer

2. Navigate into the folder from the terminal and run: 
   - Over HTTPS: 
   `$ git clone https://gitlab.com/remember-me-app/rememberme-be.git`
   
   - Over SSH:
   `$ git clone git@gitlab.com:remember-me-app/rememberme-be.git`
  

3. Set up a virtual env in your directory:

   - To set up a virtualenv, you can follow the instructions here: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

4. If you’ve set up your virtual environment as stated above, activate your virtualenv with the command:

   `$ source <env name>/bin/activate`

5. Set up database?

   - Install postgres ?

   - Log into the postgres shell:

     `$ sudo -u postgres psql`

   - Create a database:

     `$ CREATE DATABASE rememberme;`

   - Create user with **username** and **password** of choice: 

     `$ CREATE USER dbadmin WITH PASSWORD 'abc123!’;`

   - Set default encoding, transaction isolation scheme (recommended by Django)

     `$ ALTER ROLE dbadmin SET client_encoding TO 'utf8';`
     `$ ALTER ROLE dbadmin SET default_transaction_isolation TO 'read committed';`
     `$ ALTER ROLE dbadmin SET timezone TO 'UTC';`

   - Give user access to database

     `$ GRANT ALL PRIVILEGES ON DATABASE rememberme TO dbadmin;`

   - Quit out of the postgres shell

     `$ \q`

6. Run the following command from your terminal to install required packages for the project:

   `$ pip install -r requirements.txt`

7. Run Migrations: to create database tables according to database settings and models

   `$ python manage.py makemigrations`

   `$ python manage.py migrate` 

8. Create a superuser

   `$ python manage.py createsuperuser`

   - You’ll be requested to input a username, email (optional) and password for the superuser
   - This allows you the flexible option of managing data using the Django Admin panel

9. Run Server

    `$ python manage.py runserver 0.0.0.0:8000`

10. In a browser of your choice, load up the API interface at:

    `http://server_domain_or_IP:8000`
